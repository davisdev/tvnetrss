<?php

namespace App\Providers;

use App\Contracts\ShouldParseXml;
use App\Utilities\TvnetXmlParser;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // When we ask for specific contract, use these implementations application wide.
        // Without setting these IoC container has no idea how to resolve interfaces.
        $this->app->bind(ShouldParseXml::class, TvnetXmlParser::class);
    }
}
