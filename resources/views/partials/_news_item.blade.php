<div class="card flex-row flex-wrap mb-3">
    <div class="card-header border-0 w-100">
        <img src="{{ $newsItem->image_url }}" alt="{{ $newsItem->title }}">
    </div>
    <div class="card-block px-2">
        <h4 class="card-title">{{ $newsItem->title }}</h4>
        <p class="card-text">{{ $newsItem->description }}</p>
        <a href="{{ $newsItem->url }}" class="btn btn-primary mb-3">Open</a>
    </div>
</div>
