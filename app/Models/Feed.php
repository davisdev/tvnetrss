<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

class Feed extends Model
{
    /**
     * @var array
     */
    protected $casts = [
        'published' => 'datetime'
    ];
    /**
     * @var array
     */
    protected $fillable = [
        'hash',
        'published'
    ];

    /**
     * @var int 
     */
    private static $defaultNewsAmount = 5;

    /**
     * Feed can have many news items.
     *
     * @return HasMany
     */
    public function news()
    {
        return $this->hasMany(News::class);
    }

    /**
     * Wrapper for getting single latest feed.
     *
     * @param  string  $byField
     *
     * @return mixed
     */
    public static function recent(string $byField = 'created_at')
    {
        return static::latest($byField)->first();
    }

    /**
     * @return mixed
     */
    public static function latestNews()
    {
        if (!$feed = static::recent()) {
            return collect();
        }

        return $feed->news()->latest('created_at')
                    ->limit(config('services.tvnet.news_limit', static::$defaultNewsAmount))
                    ->get();
    }

    /**
     * @param  Carbon  $feedPublishDate
     *
     * @return bool
     */
    public function shouldCreateNewFeed(Carbon $feedPublishDate)
    {
        $latestFeed = $this->latest('created_at')->first();

        return !$latestFeed ||
               !$latestFeed->news()->count() ||
               $feedPublishDate->gt($latestFeed->latest('created_at')->first()->created_at);
    }

    /**
     * @param  Collection  $feedItems
     * @param  Carbon|null  $publishDate
     *
     * @return mixed
     */
    public function createFromRss(Collection $feedItems, Carbon $publishDate = null)
    {
        $date = $publishDate ?: now();

        $feed = $this->create([
            'hash'      => Hash::make($date->toDateTimeString()),
            'published' => $date->toDateTimeString()
        ]);

        $feedItems->each(function ($attributes) use ($feed) {
            return $feed->news()->create($attributes);
        });

        $this->clearOldFeeds($feed->hash);

        return $feed->news;
    }

    /**
     * @param string $exceptFor Feeds (by hash) that should not get removed.
     */
    public function clearOldFeeds(string $exceptFor)
    {
        $this->where('hash', '!=', $exceptFor)->delete();
    }
}
