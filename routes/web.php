<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth', 'verified']], function () {
   Route::get('/', [HomeController::class, 'index']);
});

Route::post('check-email',
    [RegisterController::class, 'checkEmailExistence']
)->middleware('guest');
