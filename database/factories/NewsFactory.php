<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\News;
use Faker\Generator as Faker;

$factory->define(News::class, function (Faker $faker) {
    return [
        'guid' => 'pm#' . $faker->randomNumber(4),
        'title' => $faker->sentence,
        'description' => $faker->sentence(10),
        'url' => $faker->url,
        'image_url' => $faker->imageUrl()
    ];
});
