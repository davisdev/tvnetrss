require('./bootstrap');

window.Vue = require('vue');

// For e-mail validation.
let emailFormat = new RegExp("[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")

/**
 * Provide URL for any ajax.
 */
function currentUrl() {
    let protocol = window.location.protocol
    let host = window.location.hostname
    let port = window.location.port !== 80 ? ':' + window.location.port : ''

    return protocol + '//' + host + port
}

window.ajaxUrl = currentUrl()


new Vue({
    el: '#app',
    data: {
        email: {
            value: null,
            error: null,
            isAvailable: false,
        }
    },

    methods: {
        checkEmailExistence() {
            if (this.isEmailFieldEmpty()) return

            if (! this.isLegitEmailAddress(this.email.value)) {
                this.email.error = "Please, provide a proper e-mail address"

                return
            }

            this.checkEmailAvailability()
            this.clearError()
        },


        /**
         * Validate if e-mail is correctly formatted.
         *
         * @param email
         * @returns {boolean}
         */
        isLegitEmailAddress(email) {
            return emailFormat.test(email)
        },

        /**
         * @returns {boolean}
         */
        isEmailAvailable() {
            return this.email.value && this.email.isAvailable
        },

        /**
         * Clear e-mail errors.
         */
        clearError() {
            if (this.email.error) {
                this.email.error = null
            }
        },

        isEmailFieldEmpty() {
            return !this.email.value
        },

        checkEmailAvailability() {
            window.axios.post(ajaxUrl + '/check-email', {
                email: this.email.value
            }).then(() => {
                this.email.isAvailable = true
            })
                .catch(error => this.registerFormError(error))
        },

        registerFormError(error) {
            this.email.isAvailable = false
            this.email.error = error.response.data.errors.email[0]
        }
    }
});
