@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Latest news from TVNET</div>

                <div class="card-body">
                    @forelse ($news as $newsItem)
                        @include('partials._news_item')
                    @empty
                        No latest news found.
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
