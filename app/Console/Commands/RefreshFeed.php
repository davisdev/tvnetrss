<?php

namespace App\Console\Commands;

use App\Contracts\ShouldParseXml;
use App\Models\Feed;
use Carbon\Carbon;
use Exception;
use Illuminate\Config\Repository;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class RefreshFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh RSS feeds in case there is update on external RSS.';

    /**
     * @var
     */
    private $parser;

    /**
     * @var
     */
    private $feed;

    /**
     * @var Repository|mixed
     */
    private $url;

    /**
     * @var bool
     */
    private $transformDates = true;

    /**
     * Create a new command instance.
     *
     * @param  ShouldParseXml  $parser
     * @param  Feed  $feed
     *
     * @throws Exception
     */
    public function __construct(ShouldParseXml $parser, Feed $feed)
    {
        parent::__construct();

        $this->url = config('services.tvnet.url');
        $this->parser = $parser;
        $this->feed = $feed;

        $this->healthCheck();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $xmlItems = $this->parser->forString(
            Http::get($this->url)->body()
        )->items();

        $shouldCreateNewFeed = $this->feed->shouldCreateNewFeed(
            $this->parser->getPublishDate(true)
        );

        if (!$shouldCreateNewFeed) {
            return;
        }

        $newsItems = [];

        for ($iteration = 0; $iteration < $this->parser->count(); $iteration++) {
            $newsItems[] = $xmlItems[$iteration];
        }

        $this->feed->createFromRss(
            $this->transform($newsItems), $this->parser->getPublishDate(true)
        );
    }

    /**
     * Double-checking if settings are defined.
     *
     * @throws Exception
     */
    private function healthCheck()
    {
        if (!$this->url) {
            throw new Exception(
                "No URL specified for Http client."
            );
        }
    }

    /**
     * Get image provided for given RSS feed item.
     *
     * @param $item
     *
     * @return string
     */
    private function getImage($item)
    {
        return (string) $item['enclosure']['url'];
    }

    /**
     * @param  array  $newsItems
     *
     * @return Collection
     */
    private function transform(array $newsItems)
    {
        $transformedItems = collect($newsItems)->map(function ($item) {
            $filteredAttributes =  Arr::except((array) $item, ['category']);

            return $this->mapAttributes($filteredAttributes);
        });

        if ($this->transformDates) {
            $transformedItems->transform(function ($item) {
                $item['published'] = Carbon::parse($item['published'])->format('Y-m-d H:i:s');

                return $item;
            });
        }

        return $transformedItems;
    }

    /**
     * Map feed news attributes in more readable format.
     *
     * @param  array  $attributes
     *
     * @return array
     */
    private function mapAttributes(array $attributes)
    {
        return [
            'title' => $attributes['title'],
            'description' => $attributes['description'],
            'url' => $attributes['link'],
            'guid' => $attributes['guid'],
            'published' => $attributes['pubDate'],
            'image_url' => $this->getImage($attributes),
        ];
    }
}
