<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class News extends Model
{
    /**
     * @var array
     */
    protected $casts = [
        'published' => 'datetime',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'guid',
        'title',
        'description',
        'url',
        'image_url',
        'published',
    ];

    /**
     * Many news can have one feed.
     *
     * @return BelongsTo
     */
    public function feed()
    {
        return $this->belongsTo(Feed::class);
    }
}
