<?php

namespace App\Contracts;

interface ShouldParseXml
{
    /**
     * Show information about the feed.
     *
     * @return mixed
     */
    public function metadata();

    /**
     * @param $xml
     *
     * @return mixed
     */
    public function forString($xml);

    /**
     * @param $xml
     *
     * @return mixed
     */
    public function forDocument($xml);

    /**
     * Get main items from the given feed
     * which are iterable.
     *
     * @return mixed
     */
    public function items();
}
