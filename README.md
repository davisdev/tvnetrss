## Installation
1. Clone the repo.
2. `cp .env.example .env`
3. `composer install`
4. `php artisan key:generate`
5. `npm install && npm run dev`
6. Create a database and put its credentials in `.env`
7. `php artisan migrate` (optionally add `--seed` flag, serves no real purpose other than seeding a dummy feed and user)
8. `php artisan serve` to run the server
9. `cd <project-folder> && sudo chmod +x scheduler.sh && ./scheduler.sh` to run scheduler for feed updates
