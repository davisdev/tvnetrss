<?php

namespace App\Utilities;

use App\Contracts\ShouldParseXml;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class TvnetXmlParser implements ShouldParseXml
{
    /**
     * @var
     */
    protected $xml;

    /**
     * @param $xml
     *
     * @return $this
     * @throws \Exception
     */
    public function forString($xml)
    {
        $this->xml = simplexml_load_string($xml);
        $this->isXmlValid();

        return $this;
    }

    /**
     * @param $xml
     *
     * @return $this
     * @throws \Exception
     */
    public function forDocument($xml)
    {
        $this->xml = simplexml_load_file($xml);
        $this->isXmlValid();

        return $this;
    }

    /**
     * @return mixed
     */
    public function items()
    {
        return $this->xml->children()->channel->item;
    }

    /**
     * @inheritDoc
     */
    public function metadata()
    {
        $xmlArray = (array) $this->xml->channel;

        return Arr::except($xmlArray, ['image', 'item']);
    }

    /**
     * Get RSS feed publish date timestamp.
     *
     * @param  bool  $asCarbonInstance
     *
     * @return string
     */
    public function getPublishDate(bool $asCarbonInstance = false)
    {
        $publishedDate = Carbon::parse($this->metadata()['pubDate']);

        if (!$asCarbonInstance) {
            return $publishedDate->toDateTimeString();
        }

        return $publishedDate;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->xml->channel->item);
    }

    /**
     * @throws \Exception
     */
    private function isXmlValid()
    {
        if (!$this->xml) throw new \Exception('Invalid XML loaded.');
    }
}
