<?php

namespace App\Http\Controllers;

use App\Models\Feed;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', ['news' => Feed::latestNews()]);
    }
}
