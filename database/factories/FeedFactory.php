<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Feed;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

$factory->define(Feed::class, function (Faker $faker) {
    return [
        'hash' => Hash::make($faker->randomNumber(5)),
        'published' => now(),
    ];
});
