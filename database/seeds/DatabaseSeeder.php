<?php

use App\Models\Feed;
use App\Models\News;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'email'    => 'davisnaglis2@gmail.com',
            'password' => Hash::make('admin123'),
        ]);

        factory(Feed::class, 3)
            ->create()
            ->each(function (Feed $feed) {
               $feed->news()->saveMany(
                   factory(News::class, 5)->make()
               );
            });
    }
}
